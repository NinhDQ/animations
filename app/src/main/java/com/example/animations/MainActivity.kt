package com.example.animations

import android.app.Activity
import android.os.Bundle
import android.view.View
import android.view.animation.AnimationUtils
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : Activity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val animFadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        val animFadeOut = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_out)
        val animCrossFadeIn = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_in)
        val animCrossFadeOut = AnimationUtils.loadAnimation(applicationContext, R.anim.fade_out)
        val animBlink = AnimationUtils.loadAnimation(applicationContext, R.anim.blink)
        val animZoomIn = AnimationUtils.loadAnimation(applicationContext, R.anim.zoom_in)
        val animZoomOut = AnimationUtils.loadAnimation(applicationContext, R.anim.zoom_out)
        val animRotate = AnimationUtils.loadAnimation(applicationContext, R.anim.rotate)
        val animMove = AnimationUtils.loadAnimation(applicationContext, R.anim.move)
        val animSlideUp = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_up)
        val animSlideDown = AnimationUtils.loadAnimation(applicationContext, R.anim.slide_down)
        val animBounce = AnimationUtils.loadAnimation(applicationContext, R.anim.bounce)
        val animSequential = AnimationUtils.loadAnimation(applicationContext, R.anim.sequential)
        val animTogether = AnimationUtils.loadAnimation(applicationContext, R.anim.together)

        btnFadeIn.setOnClickListener {
            txt_fade_in.visibility = View.VISIBLE
                txt_fade_in.startAnimation(animFadeIn)
        }
        btnFadeOut.setOnClickListener {
            txt_fade_out.visibility = View.VISIBLE
            txt_fade_out.startAnimation(animFadeOut)
        }
        btnCrossFade.setOnClickListener {
            txt_out.visibility = View.VISIBLE
            // start fade in animation
            txt_out.startAnimation(animCrossFadeIn)

            // start fade out animation
            txt_in.startAnimation(animCrossFadeOut)
        }
        btnBlink.setOnClickListener {
            txt_blink.visibility = View.VISIBLE
            txt_blink.startAnimation(animBlink)
        }
        btnZoomIn.setOnClickListener {
            txt_zoom_in.visibility = View.VISIBLE
            txt_zoom_in.startAnimation(animZoomIn)
        }
        btnZoomOut.setOnClickListener {
            txt_zoom_out.visibility = View.VISIBLE
            txt_zoom_out.startAnimation(animZoomOut)
        }
        btnRotate.setOnClickListener {
            txt_rotate.startAnimation(animRotate)
        }
        btnMove.setOnClickListener {
            txt_move.startAnimation(animMove)
        }
        btnSlideUp.setOnClickListener {
            txt_slide_up.startAnimation(animSlideUp)
        }
        btnSlideDown.setOnClickListener {
            txt_slide_down.startAnimation(animSlideDown)
        }
        btnBounce.setOnClickListener {
            txt_bounce.startAnimation(animBounce)
        }
        btnSequential.setOnClickListener {
            txt_seq.startAnimation(animSequential)
        }
        btnTogether.setOnClickListener {
            txt_tog.startAnimation(animTogether)
        }
    }
}
